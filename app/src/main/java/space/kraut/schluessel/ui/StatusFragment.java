// Copyright (c) 2018, Philipp Matthias Schäfer (philipp.matthias.schaefer@posteo.de)
//
// This file is part of Krautschlüssel.
//
// Krautschlüssel is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Krautschlüssel is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with Kautschlüssel. If not, see <https://www.gnu.org/licenses/>.
package space.kraut.schluessel.ui;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import space.kraut.schluessel.R;
import space.kraut.schluessel.token.TokenError;
import space.kraut.schluessel.token.EnterTokenDialogFragment;
import space.kraut.schluessel.token.TokenErrorDialogFragment;
import space.kraut.schluessel.shared.AbstractChangeNotifyingService;
import space.kraut.schluessel.token.TokenReadService;
import space.kraut.schluessel.token.TokenScannerActivity;
import space.kraut.schluessel.token.TokenService;
import space.kraut.schluessel.wifi.WifiStatusService;

public class StatusFragment extends Fragment implements AbstractChangeNotifyingService.ChangeListener {

    private static final int SELECT_TOKEN_FILE_ACTIVITY_REQUEST = 0x1;
    private static final int SCAN_TOKEN_ACTIVITY_REQUEST = 0x2;

    private static final int READ_EXTERNAL_STORAGE_PERMISSION_REQUEST = 0x1;
    private static final int CAMERA_PERMISSION_REQUEST = 0x2;

    private TokenService tokenService;
    private TokenReadService tokenReadService;
    private WifiStatusService wifiStatusService;
    private Uri tokenFileUri;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        tokenService = TokenService.getInstance();
        tokenService.addListener(this);

        tokenReadService = new TokenReadService(tokenService);

        wifiStatusService = WifiStatusService.getInstance();
        wifiStatusService.addListener(this);

        wifiStatusService.refreshStatus(context);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.status_fragment, container, false);

        View openWifiSettingsButton = view.findViewById(R.id.open_wifi_settings_button);
        openWifiSettingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }
        });

        View refreshWifiStatusButton = view.findViewById(R.id.refresh_wifi_status_button);
        refreshWifiStatusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refreshWifiStatus();
            }
        });

        View scanTokenButton = view.findViewById(R.id.scan_token_button);
        if(getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            scanTokenButton.setVisibility(View.VISIBLE);
            scanTokenButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST);
                    } else {
                        startActivityForTokenScan();
                    }
                }
            });
        } else {
            scanTokenButton.setVisibility(View.GONE);
        }

        View readTokenButton = view.findViewById(R.id.read_token_button);
        readTokenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    startActivityForTokenFile();
            }
        });

        View enterTokenButton = view.findViewById(R.id.enter_token_button);
        enterTokenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openEnterTokenDialog();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshWifiStatus();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Activity.RESULT_CANCELED == resultCode) {
            return;
        }

        FragmentActivity activity = getActivity();
        if(activity == null) {
            return;
        }

        switch(requestCode) {

            case SELECT_TOKEN_FILE_ACTIVITY_REQUEST:
                tokenFileUri = data.getData();

                if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_EXTERNAL_STORAGE_PERMISSION_REQUEST);
                } else {
                    setTokenFromFile();
                }
                break;
            case SCAN_TOKEN_ACTIVITY_REQUEST:
                try {
                    tokenService.setToken(activity.getApplicationContext(), data.getStringExtra(TokenScannerActivity.SCANNED_TOKEN));
                } catch (TokenError error) {
                    TokenErrorDialogFragment.open(activity, error);
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode) {
            case READ_EXTERNAL_STORAGE_PERMISSION_REQUEST:
                if(grantResults.length > 0 && PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    setTokenFromFile();
                } else {
                    new AlertDialog.Builder(getContext())
                            .setTitle(R.string.read_external_storage_required_title)
                            .setMessage(R.string.read_external_storage_required_message)
                            .setNeutralButton(R.string.close, null)
                            .create()
                            .show();
                }
                break;
            case CAMERA_PERMISSION_REQUEST:
                if(grantResults.length > 0 && PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                    startActivityForTokenScan();
                } else {
                    new AlertDialog.Builder(getContext())
                            .setTitle(R.string.camera_required_title)
                            .setMessage(R.string.camera_required_message)
                            .setNeutralButton(R.string.close, null)
                            .create()
                            .show();
                }
                break;
        }
    }

    @Override
    public void notifyChanged() {
        updateTokenStatus();
        updateWifiStatus();
    }

    private void startActivityForTokenFile() {
        Intent getContentIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getContentIntent.setType("*/*");
        getContentIntent.addCategory(Intent.CATEGORY_OPENABLE);

        String title = getResources().getString(R.string.select_token_file_title);
        Intent chooserIntent = Intent.createChooser(getContentIntent, title);
        startActivityForResult(chooserIntent, SELECT_TOKEN_FILE_ACTIVITY_REQUEST);
    }

    private void startActivityForTokenScan() {
        Intent scanTokenIntent = new Intent(getActivity(), TokenScannerActivity.class);
        startActivityForResult(scanTokenIntent, SCAN_TOKEN_ACTIVITY_REQUEST);

    }

    private void setTokenFromFile() {
        FragmentActivity activity = getActivity();
        if(activity == null) {
            return;
        }

        Context context = activity.getApplicationContext();

        try {
            tokenReadService.readAndSetToken(context, tokenFileUri);
        } catch (TokenError error) {
            TokenErrorDialogFragment.open(activity, error);
        }
    }

    private void openEnterTokenDialog() {
        FragmentActivity activity = getActivity();
        if(activity == null) {
            return;
        }

        FragmentManager fragmentManager = activity.getSupportFragmentManager();

        new EnterTokenDialogFragment().show(fragmentManager, null);
    }

    private void refreshWifiStatus() {
        FragmentActivity activity = getActivity();
        if(activity == null) {
            return;
        }

        Context context = activity.getApplicationContext();

        wifiStatusService.refreshStatus(context);
    }


    private void updateWifiStatus() {
        FragmentActivity activity = getActivity();
        if(activity == null) {
            return;
        }

        TextView wifiStatus = activity.findViewById(R.id.wifi_status);
        if(wifiStatus == null) {
            return;
        }

        switch(wifiStatusService.getStatus()) {
            case CORRECT_NETWORK:
                wifiStatus.setText(R.string.wifi_status_correct_network);
                wifiStatus.setTextColor(ContextCompat.getColor(activity, R.color.positive));
                break;
            case WRONG_NETWORK:
                wifiStatus.setText(R.string.wifi_status_wrong_network);
                wifiStatus.setTextColor(ContextCompat.getColor(activity, R.color.negative));
                break;
            case NOT_CONNECTED:
                wifiStatus.setText(R.string.wifi_status_not_connected);
                wifiStatus.setTextColor(ContextCompat.getColor(activity, R.color.negative));
                break;
            case NOT_ENABLED:
                wifiStatus.setText(R.string.wifi_status_not_enabled);
                wifiStatus.setTextColor(ContextCompat.getColor(activity, R.color.negative));
                break;
        }
    }

    private void updateTokenStatus() {
        FragmentActivity activity = getActivity();
        if(activity == null) {
            return;
        }

        TextView tokenStatus = activity.findViewById(R.id.token_status);
        if (tokenStatus == null) {
            return;
        }

        Context context = activity.getApplicationContext();
        String token = tokenService.getToken(context);

        if(token == null) {
            tokenStatus.setTextColor(ContextCompat.getColor(activity, R.color.negative));
            tokenStatus.setText(R.string.token_status_no_token_configured);
        } else {
            tokenStatus.setTextColor(ContextCompat.getColor(activity, R.color.positive));
            tokenStatus.setText(R.string.token_status_token_configured);
        }
    }

}
