// Copyright (c) 2018, Philipp Matthias Schäfer (philipp.matthias.schaefer@posteo.de)
//
// This file is part of Krautschlüssel.
//
// Krautschlüssel is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Krautschlüssel is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with Kautschlüssel. If not, see <https://www.gnu.org/licenses/>.
package space.kraut.schluessel.command;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import space.kraut.schluessel.R;
import space.kraut.schluessel.token.TokenService;

public class CommandService {

    public enum Command {

        OUTDOOR_BUZZ("outdoor_buzz"),
        INDOOR_OPEN("indoor_open"),
        INDOOR_LOCK("indoor_lock");

        @NonNull
        private final String name;

        Command(@NonNull String name) {
            this.name = name;
        }

        @NonNull
        private String getName() {
            return name;
        }

    }

    private static class CommandTask extends AsyncTask<URL, Void, CommandResult> {

        private final FragmentActivity activity;

        private CommandTask(FragmentActivity activity) {
            this.activity = activity;
        }

        @Override
        protected CommandResult doInBackground(URL... urls) {
            try {
                HttpsURLConnection connection = (HttpsURLConnection) urls[0].openConnection();
                connection.connect();
                int response = connection.getResponseCode();
                if(!(200 <= response && response < 400)) {
                    return CommandResult.ERROR_RESPONSE;
                }
            } catch (IOException e) {
                return CommandResult.CONNECTION_ERROR;
            }
            return CommandResult.SUCCESS;
        }

        @Override
        protected void onPostExecute(CommandResult result) {
            if(CommandResult.SUCCESS != result) {
                CommandErrorDialogFragment.open(activity, result);
            }
        }
    }

    private static final String URL_PATTERN = "https://tuer.kraut.space/?cmd=%s&secret=%s";

    private final TokenService tokenService;

    public CommandService(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    public void sendCommand(@NonNull FragmentActivity activity, @NonNull Command command) {
        Context context = activity.getApplicationContext();
        String token = tokenService.getToken(context);

        String commandName = command.getName();
        String urlString = String.format(URL_PATTERN, commandName, token);

        try {
            URL url = new URL(urlString);
            new CommandTask(activity).execute(url);
        } catch (MalformedURLException e) {
            throw new RuntimeException(context.getString(R.string.unexpected_error_message));
        }
    }

}
