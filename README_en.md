# Krautschlüssel

Krautschlüssel is an Android application for locking and unlocking Jena
Krautspace's door.

## Licenses

This application was written by Philipp Matthias Schäfer
(philipp.matthias.schaefer@posteo.de) and published under the GPL3 license. See
[LICENSE](LICENSE) for the license's text.

Krautschlüssel uses icons from Google's Material Design page:

https://material.io/tools/icons/

The icons were published by Google under the Apache License, Version 2.0. See

https://www.apache.org/licenses/LICENSE-2.0.html

for the license's text.

Krautschlüssel uses several libraries. They were all published by their
respective copyright holders under the Apache License, Version 2.0. See

https://www.apache.org/licenses/LICENSE-2.0.html

for the license's text.

The following is a list of these libraries with links to their respective source
code.

* Barcode Scanner View/ZXing Scanner View:
  https://github.com/dm77/barcodescanner

* ZXing
  https://github.com/zxing/zxing

* AOSP Support Library/Android Lifecycle Runtime/Android Arch-Common
  https://android.googlesource.com/platform/frameworks/support
